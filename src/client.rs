use anyhow::{Context, Error};
use tokio::io::AsyncReadExt;
use tracing::info;
use veilid_core::tools::*;
use veilid_core::{api_startup, CryptoTyped, KeyPair, VeilidUpdate};

use crate::config::config_callback;
use crate::utils::{wait_for_attached, wait_for_network_start, wait_for_public_internet_ready};
use crate::MAX_APP_MESSAGE_MESSAGE_LEN;

fn update_callback(update: VeilidUpdate) {
    match update {
        _ => (),
    }
}

pub async fn client(
    temp_dir: std::path::PathBuf,
    key_pair: CryptoTyped<KeyPair>,
    blob: Vec<u8>,
) -> Result<(), Error> {
    let mut stdin = tokio::io::stdin();

    let temp_dir_clone = temp_dir.clone();
    let api = api_startup(
        Arc::new(update_callback),
        Arc::new(move |key| config_callback(temp_dir_clone.clone(), key_pair.clone(), key)),
    )
    .await
    .expect("startup failed");

    api.attach().await?;

    wait_for_network_start(&api).await;

    let attachment_manager = api.attachment_manager()?;
    wait_for_attached(&attachment_manager).await;

    let rc = api.routing_context().with_privacy()?;

    wait_for_public_internet_ready(&attachment_manager).await?;

    let route_id = api.import_remote_private_route(blob).unwrap();
    let target = veilid_core::Target::PrivateRoute(route_id);

    let mut buf = [0; MAX_APP_MESSAGE_MESSAGE_LEN];

    info!("sending file");

    loop {
        let len = stdin.read(&mut buf).await?;
        if len == 0 {
            break;
        }
        rc.app_message(target.clone(), buf[..len].to_vec())
            .await
            .context("app_message")?;
    }

    // send empty payload to finish
    rc.app_message(target.clone(), vec![])
        .await
        .context("app_message")?;

    info!("sending file, done");

    api.shutdown().await;

    Ok(())
}

{ pkgs, pre-commit-hooks, system, myRust }:

with pkgs;

let
  pre-commit-check = pre-commit-hooks.lib.${system}.run {
    src = ./.;
    hooks = {
      # cargo-check.enable = true;
      deadnix.enable = true;
      nixpkgs-fmt.enable = true;
      rustfmt.enable = true;
      statix.enable = true;
    };
    excludes = [
    ];
  };

in
mkShell {
  nativeBuildInputs = [
    myRust
    pkg-config
  ];

  buildInputs = [
    capnproto
    cargo-outdated
    openssl
    protobuf
  ];

  RUST_SRC_PATH = "${myRust}/lib/rustlib/src/rust/library";

  shellHook = ''
    ${pre-commit-check.shellHook}
  '';
}
